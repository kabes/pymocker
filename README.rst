pyMocker
========

Simple, configurable server for mocking web services. No external
dependencies.

Running
-------

1. prepare pymocker.cfg
2. run::

    pymocker




Licence
-------

Copyright (c) Karol Będkowski, 2024

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For details please see COPYING file.

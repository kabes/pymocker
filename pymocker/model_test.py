# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
from pymocker import model

"""

"""


def test_service_can_handle_methods() -> None:
    srv = model.StdService("", [])
    srv.methods = {"GET", "POST"}

    req = model.Request(method="GET", path="/test", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="POST", path="/test", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="PUT", path="/test", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    srv.methods = {}  # type: ignore

    req = model.Request(method="GET", path="/test", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="POST", path="/test", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="PUT", path="/test", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)


def test_service_can_handle_path() -> None:
    # handle any path
    srv = model.StdService("", [])

    req = model.Request(method="GET", path="/test", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="Post", path="/", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    # plain
    srv = model.StdService("", [r"^/test/123$"])

    req = model.Request(method="GET", path="/test/123", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(
        method="GET", path="/test/123/123", headers={}, reqid=0  # type: ignore
    )
    assert not srv.can_handle(req)

    req = model.Request(method="GET", path="/test/1234", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    req = model.Request(method="GET", path="/test/", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    req = model.Request(method="GET", path="/test", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    # regexp
    srv = model.StdService("", [r"^/test/\d+$"])

    req = model.Request(method="GET", path="/test/123", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="GET", path="/test/abc", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    req = model.Request(method="GET", path="/test", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    # multi
    srv = model.StdService("", [r"^/test/\d+$", "^/abc/$"])

    req = model.Request(method="GET", path="/test/123", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="POST", path="/abc/", headers={}, reqid=0)  # type: ignore
    assert srv.can_handle(req)

    req = model.Request(method="POST", path="test", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)


def test_service_can_handle_headers() -> None:
    # any = exists
    srv = model.StdService("", [])
    srv.headers = {"abc": "<ANY>"}

    req = model.Request(method="POST", path="test", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    req = model.Request(
        method="POST", path="test", headers={"abc": ["a"]}, reqid=0  # type: ignore
    )
    assert srv.can_handle(req)

    req = model.Request(
        method="POST", path="test", headers={"abc": [""]}, reqid=0  # type: ignore
    )
    assert srv.can_handle(req)

    # given
    srv.headers = {"abc": "123", "qwe": "test"}

    req = model.Request(method="POST", path="test", headers={}, reqid=0)  # type: ignore
    assert not srv.can_handle(req)

    req = model.Request(
        method="POST", path="test", headers={"abc": ["123"]}, reqid=0  # type: ignore
    )
    assert not srv.can_handle(req)

    req = model.Request(
        method="POST",
        path="test",
        headers={"abc": ["123"], "qwe": ["test"]},  # type: ignore
        reqid=0,
    )
    assert srv.can_handle(req)

    req = model.Request(
        method="POST",
        path="test",
        headers={"abc": ["12", "123"], "qwe": ["test"]},  # type: ignore
        reqid=0,
    )
    assert srv.can_handle(req)

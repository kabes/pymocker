# Copyright © 2024 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

from __future__ import annotations

import argparse
import logging
import sys

from pymocker import config, log, server

try:
    import icecream
except ImportError:
    pass
else:
    icecream.install()


__all__ = ["main"]

_LOG = logging.getLogger("cli")


def main() -> None:
    parser = argparse.ArgumentParser(
        prog="pyMocker", description="Mock web services"
    )
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument("-s", "--silent", action="count", default=0)
    parser.add_argument(
        "-c", "--config", help="configuration file", default="pymocker.cfg"
    )

    args = parser.parse_args()

    log.setup_logging(args.verbose - args.silent)

    cfg = config.load_configuration(args.config)
    _LOG.debug("cfg: %r", cfg)
    if not config.validate_configuration(cfg):
        _LOG.error("invalid configuration")
        sys.exit(1)

    server.run(cfg)

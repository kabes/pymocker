# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Logging setup.
"""
import logging
import sys


class ColorFormatter(logging.Formatter):
    """Formatter for logs that color messages according to level."""

    FORMAT_MAP = {
        levelno: f"\033[1;{color}m{level:<8}\033[0m"
        for levelno, level, color in (
            (logging.DEBUG, "DEBUG", 34),
            (logging.INFO, "INFO", 37),
            (logging.WARNING, "WARNING", 33),
            (logging.ERROR, "ERROR", 31),
            (logging.CRITICAL, "CRITICAL", 31),
        )
    }

    def format(self, record: logging.LogRecord) -> str:
        record.levelname = self.FORMAT_MAP.get(
            record.levelno, record.levelname
        )
        return logging.Formatter.format(self, record)


def setup_logging(level: int) -> None:
    log = logging.getLogger(None)
    if level > 0:
        # debug
        log.setLevel(logging.DEBUG)
    elif level == -1:
        log.setLevel(logging.WARN)
    elif level < -1:
        log.setLevel(logging.ERROR)
    else:
        log.setLevel(logging.INFO)

    console = logging.StreamHandler()
    fmtr = logging.Formatter
    if sys.platform != "win32" and sys.stdout.isatty():
        fmtr = ColorFormatter

    if level > 1:
        msg_format = (
            "%(levelname)-8s %(name)-8s [%(filename)s:%(lineno)d] %(message)s"
        )
    else:
        msg_format = "%(levelname)-8s %(name)-8s %(message)s"

    if not sys.stdout.isatty():
        msg_format = "%(asctime)s " + msg_format

    console.setFormatter(fmtr(msg_format))
    log.addHandler(console)

    logging.getLogger("main").info("starting...")

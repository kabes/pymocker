# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
XID-like id generator; inspired by: https://github.com/graham/python_xid/

"""
import base64
import hashlib
import os
import platform
import secrets
import threading
import time


def _get_machineid() -> bytes:
    hostname = platform.node()
    return hashlib.md5(hostname.encode()).digest()


class XIDGenerator:
    """Generator for 20-characters long id."""

    def __init__(self) -> None:
        self._lock = threading.Lock()
        self._value = secrets.randbits(24)
        pid = (os.getpid() & 0xFFFF).to_bytes(2, "big")
        self._machine_id_pid = _get_machineid()[:3] + pid

    def get(self) -> str:
        with self._lock:
            rid = self._value = (self._value + 1) & 0xFFFFFF

        bid = (
            int(time.time()).to_bytes(4, "big")
            + self._machine_id_pid
            + rid.to_bytes(3, "big")
        )

        return base64.b32encode(bid).lower()[:20].decode()


class ShortIDGenerator:
    """Generator for 12-characters long id."""

    def __init__(self) -> None:
        self._lock = threading.Lock()
        self._value = secrets.randbits(16)
        self._pid = (os.getpid() & 0xFFFF).to_bytes(2, "big")

    def get(self) -> str:
        with self._lock:
            rid = self._value = (self._value + 1) % 0xFFFF

        tsp = (int(time.time()) - 1577833200) & 0xFFFFFF
        bid = tsp.to_bytes(3, "big") + self._pid + rid.to_bytes(2, "big")

        return base64.urlsafe_b64encode(bid)[:10].decode()


xid_generator = ShortIDGenerator()

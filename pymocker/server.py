# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""

"""

import json
import logging
import ssl
import typing as ty
from contextlib import suppress
from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer

from pymocker import model, xid

_LOG = logging.getLogger("server")


class ServicesHandler(BaseHTTPRequestHandler):
    def __init__(self, *args: ty.Any, **kwarg: ty.Any) -> None:
        self.reqid: str = xid.xid_generator.get()
        super().__init__(*args, **kwarg)

    def log_message(self, format: str, *args: ty.Any) -> None:
        _LOG.info(f"[reqid={self.reqid}] {format}", *args)

    def log_debug(self, format: str, *args: ty.Any) -> None:
        _LOG.debug(f"[reqid={self.reqid}] {format}", *args)

    def log_error(self, format: str, *args: ty.Any) -> None:
        _LOG.error(f"[reqid={self.reqid}] {format}", *args)

    @property
    def cfg(self) -> model.AppConfig:
        return self.server.cfg  # type: ignore

    def __log_req(self, req: model.Request, as_err: bool = False) -> None:
        if as_err:
            lhandler = self.log_error
        elif self.cfg.log_requests:
            lhandler = self.log_message
        else:
            lhandler = self.log_debug

        cert = None
        if req.peer_cert:
            try:
                cinfo = [
                    f"cn:{v}"
                    for k, v in req.peer_cert["subject"][0]
                    if k == "commonName"
                ]
                cinfo.append(f"sn:{req.peer_cert['serialNumber']}")
                cert = ",".join(cinfo)

            except Exception as err:
                _LOG.exception("cert info error %r: %s", req.peer_cert, err)
                cert = "<ERR>"

        lhandler(
            "IN: method=%r, path=%r, crt=%s, headers=%s",
            self.command,
            self.path,
            cert,
            model.repr_dict(self.headers),  # type: ignore
        )
        lhandler("IN: body=%r", req.body)

    def __log_resp(self, resp: model.Response, as_err: bool = False) -> None:
        if as_err:
            lhandler = self.log_error
        elif self.cfg.log_responses:
            lhandler = self.log_message
        else:
            lhandler = self.log_debug

        lhandler(
            "OUT: status=%r, message=%r, headers=%s",
            resp.status,
            resp.status_msg,
            model.repr_dict(resp.headers),
        )
        lhandler("OUT: body=%r", resp.body)

    def _handle(self) -> None:
        body = None
        if clen := self.headers.get("content-length"):
            body = self.rfile.read(int(clen))

        req = model.Request(
            method=self.command,
            path=self.path,
            headers=self.headers,  # type: ignore
            reqid=self.reqid,
            client=self.client_address,
            body=body,
        )

        if isinstance(self.connection, ssl.SSLSocket):
            req.peer_cert = self.connection.getpeercert()

        self.__log_req(req)

        for srv in self.cfg.services:
            if not srv.can_handle(req):
                continue

            self.log_debug("handler: service=%s", srv.name)

            resp = model.Response()
            try:
                srv.handle(req, resp)
            except Exception as err:
                _LOG.exception("[reqid=%s] handler error: %s", self.reqid, err)
                self.__log_req(req, as_err=True)
                self.send_error(500)
                return

            self.__log_resp(resp)
            self.send_response(resp.status, resp.status_msg)

            if resp.headers:
                for key, val in resp.headers.items():
                    self.send_header(key, val)

            self.send_header("X-PYMOCKER-SRV", srv.name)
            self.send_header("X-PYMOCKER-REQID", self.reqid)
            self.end_headers()

            if resp.body:
                self.wfile.write(resp.body)

            return

        self.send_error(404, "not found")

    def _handle_internal(self) -> None:
        if self.path == "/_/ready":
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b"ready")
            return

        if self.path == "/_/health":
            self.send_response(200)
            self.headers["content-type"] = "application/json"
            self.end_headers()
            res = {"health": "ok", "services": len(self.cfg.services)}
            self.wfile.write(json.dumps(res).encode())
            return

        self.send_error(404, "not found")

    def do_GET(self) -> None:
        if self.path.startswith("/_/"):
            self._handle_internal()
            return

        self._handle()

    def do_POST(self) -> None:
        self._handle()

    def do_PUT(self) -> None:
        self._handle()

    def do_PATH(self) -> None:
        self._handle()

    def do_DELETE(self) -> None:
        self._handle()


class Server(ThreadingHTTPServer):
    def __init__(self, address: tuple[str, int], cfg: model.AppConfig) -> None:
        super().__init__(address, ServicesHandler)
        self.cfg = cfg

        if cfg.enable_ssl and cfg.ssl_cert_file and cfg.ssl_key_file:
            _LOG.info("ssl enabled")
            ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
            ctx.check_hostname = False
            ctx.load_cert_chain(cfg.ssl_cert_file, cfg.ssl_key_file)
            if cfg.ssl_ca_file:
                ctx.verify_mode = (
                    ssl.CERT_REQUIRED
                    if cfg.ssl_client_verify
                    else ssl.CERT_OPTIONAL
                )
                ctx.load_verify_locations(cafile=cfg.ssl_ca_file)

            self.socket = ctx.wrap_socket(self.socket, server_side=True)


def run(cfg: model.AppConfig) -> None:
    laddr, _sep, lport = cfg.address.partition(":")

    port = 7070
    if lport:
        port = int(lport)

    laddr = laddr or "127.0.0.1"

    httpd = Server((laddr, port), cfg)

    _LOG.info("listen on %s:%s...", laddr, port)

    with suppress(KeyboardInterrupt):
        httpd.serve_forever()

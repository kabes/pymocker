# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
from __future__ import annotations

import abc
import json
import logging
import re
import typing as ty
import urllib.parse
from dataclasses import dataclass, field
from http.client import HTTPMessage
from pathlib import Path

"""

"""
_LOG = logging.getLogger("model")


def repr_dict(indata: dict[str, ty.Any] | None) -> str:
    if not indata:
        return ""

    items = (f"{k}={v!r}" for k, v in indata.items())
    return ", ".join(items)


@dataclass
class Request:
    method: str
    path: str
    headers: HTTPMessage
    reqid: str
    client: tuple[str, int]
    body: bytes | None = None
    peer_cert: dict[str, ty.Any] | None = None
    _body_json: ty.Any = None
    _query_params: dict[str, list[str]] | None = None

    def __repr__(self) -> str:
        headers = repr_dict(self.headers)  # type: ignore
        return (
            f"Request<method={self.method}, path={self.path}, "
            f"headers={headers}, requd={self.reqid}, body={self.body!r}>"
        )

    def json(self) -> ty.Any:
        """Return object decoded from json body."""
        if self.body and self._body_json is None:
            self._body_json = json.loads(self.body)

        return self._body_json

    def is_json(self) -> bool:
        """Check if content-type is json."""
        if ctype := self.headers.get("content-type"):
            return str(ctype).startswith("application/json")

        return False

    @property
    def url(self) -> tuple[str, ...]:
        return urllib.parse.urlparse(self.path)

    @property
    def query_params(self) -> dict[str, list[str]]:
        if self._query_params is not None:
            return self._query_params

        parsed = urllib.parse.urlparse(self.path)
        if query := parsed.query:
            self._query_params = urllib.parse.parse_qs(query)
        else:
            self._query_params = {}

        return self._query_params

    def query_param(self, key: str, default: str | None = None) -> str | None:
        if vals := self.query_params.get(key):
            return vals[0]

        return default


@dataclass
class Response:
    status: int = 200
    status_msg: str | None = None
    headers: dict[str, str] = field(default_factory=dict)
    body: bytes | None = None

    def __repr__(self) -> str:
        headers = repr_dict(self.headers)
        return (
            f"Respose<status={self.status}, msg={self.status_msg}, "
            f"headers={headers}, body={self.body!r}>"
        )

    def set_content_type(self, ctype: str) -> None:
        """Set 'content-type' to `ctype`."""
        self.headers["content-type"] = ctype

    def as_json(self, data: ty.Any, pretty: bool = True) -> None:
        """Dump `data` as json object into response body. Set
        'content-type' header to application/json if is not set."""

        self.body = json.dumps(data, indent=4 if pretty else None).encode()

        if not self.headers.get("content-type"):
            self.headers["content-type"] = "application/json"


class Service(abc.ABC):
    def __init__(self, name: str) -> None:
        self.name = name

    @abc.abstractmethod
    def handle(self, req: Request, resp: Response) -> None:
        ...

    @abc.abstractmethod
    def can_handle(self, req: Request) -> bool:
        ...

    def validate(self) -> bool:
        return True


class StdService(Service):
    def __init__(self, name: str, paths: ty.Iterable[str]) -> None:
        super().__init__(name)
        self.path: list[re.Pattern[str]] = [re.compile(path) for path in paths]
        self.methods: set[str] | None = None
        self.headers: dict[str, str] | None = None
        self.response_body: bytes | None = None
        self.response_file: str | None = None
        self.response_headers: dict[str, str] | None = None
        self.response_status: int = 200
        self.response_status_msg: str | None = None

    def __repr__(self) -> str:
        items = (f"{k}={v!r}" for k, v in self.__dict__.items())
        return f"Service<{', '.join(items)}>"

    def handle(self, req: Request, resp: Response) -> None:
        resp.status = self.response_status
        resp.status_msg = self.response_status_msg

        if self.response_headers:
            resp.headers = self.response_headers

        if self.response_body is not None:
            resp.body = self.response_body

        elif self.response_file:
            resp.body = Path(self.response_file).read_bytes()

    def can_handle(self, req: Request) -> bool:
        if self.methods and req.method not in self.methods:
            return False

        if self.path and not any(p.match(req.path) for p in self.path):
            return False

        if self.headers:
            for key, val in self.headers.items():
                if key not in req.headers:
                    return False

                if val != "<ANY>" and val not in req.headers[key]:
                    return False

        return True

    def validate(self) -> bool:
        is_ok = True
        if not (self.response_body or self.response_file):
            _LOG.warning(
                "service %s: not defined response body/code/file; "
                "empty body will be always returned",
                self.name,
            )

        if self.path:
            for idx, path in enumerate(self.path, 1):
                try:
                    re.compile(path)
                except Exception as err:
                    _LOG.warning(
                        "service %s: invalid path[%d]=%r: %s",
                        self.name,
                        idx,
                        self.path,
                        err,
                    )
                    is_ok = False

        if self.response_file and not Path(self.response_file).is_file():
            _LOG.warning(
                "service %s: response_file=%s not exists",
                self.name,
                self.response_file,
            )
            is_ok = False

        if self.response_status < 1:
            _LOG.warning(
                "service %s: invalid response_status: %r",
                self.name,
                self.response_status,
            )
            is_ok = False

        return is_ok


class PyService(Service):
    def __init__(self, path: str, module: ty.Any) -> None:
        super().__init__(str(getattr(module, "NAME", path)))
        self._path = path
        self._module = module

    def can_handle(self, req: Request) -> bool:
        return self._module.can_handle(req)  # type: ignore

    def handle(self, req: Request, resp: Response) -> None:
        self._module.handle(req, resp)
        # fix types
        if resp.body is not None and not isinstance(resp.body, bytes):
            resp.body = str(resp.body).encode()

    def __repr__(self) -> str:
        return f"Service<path={self._path}>"


class AppConfig:
    def __init__(self) -> None:
        self.address: str = "localhost:7070"
        self.services: list[Service] = []
        self.enable_ssl: bool = False
        self.ssl_cert_file: str | None = None
        self.ssl_key_file: str | None = None
        self.ssl_client_verify: bool = False
        self.ssl_ca_file: str | None = None
        self.data_dirs: list[str] = []
        self.log_requests: bool = False
        self.log_responses: bool = False

    def __repr__(self) -> str:
        return f"AppConfig: services={self.services!s}"

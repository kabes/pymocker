# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

from __future__ import annotations

import configparser
import inspect
import logging
import typing as ty
from pathlib import Path

from pymocker import model

"""

"""

_LOG = logging.getLogger("conf")


def _import_script(name: str) -> ty.Any:
    _LOG.debug("importing: %s", name)
    modname = name.replace("/", ".").removesuffix(".py")
    plug = __import__(modname, fromlist=modname.split(".")[-1:])
    return plug


def _str_to_items(instr: str | None, sep: str = "\n") -> ty.Iterable[str]:
    if not instr:
        return

    for item in instr.split(sep):
        if item := item.strip():
            yield item


def _str_to_headers(instr: str | None) -> ty.Iterable[tuple[str, str]]:
    for header in _str_to_items(instr):
        key, _sep, val = header.partition(":")
        key = key.strip()
        if key and _sep:
            yield key, val.strip()


def _str_to_paths_def(instr: str | None) -> ty.Iterable[str]:
    for p in _str_to_items(instr):
        if not p:
            continue

        if p[0] != "^":
            p = f"^{p}"

        if p[-1] != "$":
            p = f"{p}$"

        yield p


def _load_service(
    name: str, items: list[tuple[str, str]] | configparser.SectionProxy
) -> model.Service:
    sconfig = dict(items)

    paths = _str_to_paths_def(sconfig.get("path"))
    srv = model.StdService(name, paths)

    srv.methods = set(_str_to_items(sconfig.get("methods"), ","))
    srv.headers = dict(_str_to_headers(sconfig.get("headers")))
    srv.response_headers = dict(
        _str_to_headers(sconfig.get("response_headers"))
    )
    srv.response_file = sconfig.get("response_file")
    srv.response_status = int(sconfig.get("response_status", "200"))
    srv.response_status_msg = sconfig.get("response_status_msg")
    if body := sconfig.get("response_body"):
        srv.response_body = body.encode()

    _LOG.debug("loaded standard service %s", name)
    return srv


def _load_data_cfg(path: Path) -> ty.Iterable[model.Service]:
    config = configparser.ConfigParser()
    with path.open() as fin:
        config.read_file(fin)

    for name, sec in config.items():
        if name == "DEFAULT":
            continue

        yield _load_service(name, sec)


def _has_function(obj: ty.Any, *names: str) -> bool:
    for name in names:
        if func := getattr(obj, name, None):
            if inspect.isfunction(func):
                continue

        return False

    return True


def _load_data_py(path: Path) -> ty.Iterable[model.Service]:
    num = 0
    module = _import_script(str(path))
    if _has_function(module, "can_handle", "handle"):
        num += 1
        _LOG.debug("loaded function handler from %s", path)
        yield model.PyService(str(path), module)

    for name, obj in inspect.getmembers(module):
        if name.startswith("Service") and inspect.isclass(obj):
            if not _has_function(obj, "can_handle", "handle"):
                _LOG.warning(
                    "import %s from %s error: "
                    "missing `can_handle` or `handle`",
                    name,
                    path,
                )
                continue

            try:
                srv = obj()
                if not hasattr(srv, "name"):
                    setattr(srv, "name", name)

                if not hasattr(srv, "validate"):
                    setattr(srv, "validate", lambda: True)

                num += 1
                _LOG.debug("loaded %s handler from %s", name, path)
                yield srv

            except Exception as err:
                _LOG.exception("import %s from %s error: %s", name, path, err)

    if not num:
        _LOG.warning("file %s don't contain any valid handlers", path)


def load_configuration(config_file: str) -> model.AppConfig:
    config = configparser.ConfigParser()

    with open(config_file) as fin:
        config.read_file(fin)

    cfg = model.AppConfig()

    if "server" in config:
        scfg = config["server"]
        cfg.enable_ssl = scfg.getboolean("ssl", False)
        cfg.ssl_cert_file = scfg.get("ssl_cert_file")
        cfg.ssl_key_file = scfg.get("ssl_key_file")
        cfg.ssl_client_verify = scfg.getboolean("ssl_client_verify", False)
        cfg.ssl_ca_file = scfg.get("ssl_ca_file")
        cfg.address = scfg.get("address", "localhost:7070")

    if "logging" in config:
        scfg = config["logging"]
        cfg.log_requests = scfg.getboolean("log_requests", False)
        cfg.log_responses = scfg.getboolean("log_responses", False)

    if "data" in config:
        for name, cpath in config.items("data"):
            path = Path(cpath)
            for fname in sorted(path.glob("**/*.cfg")):
                _LOG.debug("loading: %s", fname)
                try:
                    cfg.services.extend(_load_data_cfg(fname))
                except Exception as err:
                    _LOG.exception("load file %s error: %s", fname, err)

            for fname in sorted(path.glob("**/*.py")):
                _LOG.debug("loading: %s", fname)
                try:
                    cfg.services.extend(_load_data_py(fname))
                except Exception as err:
                    _LOG.exception("load module %s error: %s", fname, err)

    else:
        _LOG.error("no `data` section in config file")

    _LOG.info("loaded %d services", len(cfg.services))

    return cfg


def validate_configuration(cfg: model.AppConfig) -> bool:
    is_ok = True

    if cfg.enable_ssl:
        if cfg.ssl_cert_file:
            if not Path(cfg.ssl_cert_file).is_file():
                _LOG.warning("ssl_cert_file %s not found", cfg.ssl_cert_file)
                is_ok = False
        else:
            _LOG.warning("ssl enabled but ssl_cert_file not defined")
            is_ok = False

        if cfg.ssl_key_file:
            if not Path(cfg.ssl_key_file).is_file():
                _LOG.warning("ssl_key_file %s not found", cfg.ssl_key_file)
                is_ok = False
        else:
            _LOG.warning("ssl enabled but ssl_key_file not defined")
            is_ok = False

        if cfg.ssl_client_verify:
            if not cfg.ssl_ca_file:
                _LOG.warning("ssl client verify but no ssl_ca_file provided")
                is_ok = False
            elif not Path(cfg.ssl_ca_file).is_file():
                _LOG.warning("ssl_cert_file %s not found", cfg.ssl_ca_file)
                is_ok = False

    if not cfg.services:
        _LOG.warning("no services defined")
        is_ok = False

    is_ok = all(srv.validate() for srv in cfg.services)
    return is_ok

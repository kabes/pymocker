# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Test service.
"""


def can_handle(req):
    if not req.path.startswith("/test/py/4"):
        return False

    if req.method != "POST":
        return False

    return b"test" in req.body


def handle(req, resp):
    resp.set_content_type("text/plain")
    resp.body = str(req.path)
    resp.status = 201
    resp.status_msg = "ok"

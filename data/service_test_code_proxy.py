# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Test service.
"""

import urllib.request


class ServiceProxy:
    """Call another url (from `url` parameter)"""

    def can_handle(self, req):
        return req.path.startswith("/test/proxy")

    def handle(seff, req, resp):
        url = req.query_param("url")
        if not url:
            resp.status = 400
            resp.body = b"missing `url` parameter"
            return

        with urllib.request.urlopen(url) as conn:
            resp.body = conn.read()

# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Test service.
"""


def can_handle(req):
    if not req.path.startswith("/test/py/2"):
        return False

    if req.method != "POST":
        return False

    if not req.is_json():
        return False

    body_json = req.json()
    return body_json.get("test") == "123"


def handle(req, resp):
    body_json = req.json()
    body_json["result"] = "ok"
    # get dict[str, list[str]] of parsed query
    body_json["query"] = req.query_params
    # get one (first) value for given query parameter
    body_json["query_arg1"] = req.query_param("arg1", "missing")
    # client is (address, port)
    body_json["client"] = f"{req.client[0]}:{req.client[1]}"
    body_json["req"] = {
        k: str(v) for k, v in req.__dict__.items() if k[0] != "_"
    }

    resp.as_json(body_json)
    # content type is set by as_json
    # resp.headers["content-type"] = "application/json"
    resp.status = 201
    resp.status_msg = "ok"


# service as class, name must start with "Service"
class ServiceTest1:
    def can_handle(self, req):
        return req.path.startswith("/test/py/3")

    def handle(seff, req, resp):
        resp.status = 200
        resp.body = b"ok"

# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Test service.
"""

import datetime
import sys


def can_handle(req):
    return req.path.startswith("/test/py/1/") and req.method in (
        "GET",
        "POST",
    )


def handle(req, resp):
    body = [
        f"now: {datetime.datetime.now()}",
        f"python {sys.version}",
        f"req method: {req.method}",
        f"req path: {req.path}",
        "req headers:",
    ]

    body.extend(f"  {k}: {v}" for k, v in req.headers.items())

    resp.headers["content-type"] = "text/plain"
    resp.body = "\n".join(body)
    resp.status = 201
    resp.status_msg = "ok"
